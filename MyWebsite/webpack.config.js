﻿/// <binding ProjectOpened='Watch - Development' />
var Webpack = require("webpack");
var Wwwroot = __dirname + "/wwwroot";
module.exports = {
    cache: true,
    entry: {
        "bundle": [Wwwroot + "/app/bundle.ts"],
        "bundle-vendors": [Wwwroot + "/app/bundle-vendors.ts"],
    },
    output: {
        path: Wwwroot + "/js",
        filename: "[name].js",
        sourceMapFilename: "[name].map"
    },
    resolve: {
        extensions: [".ts", ".js", ".html"]
    },
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loaders: [
                    "awesome-typescript-loader",
                    "angular2-template-loader"
                ],
                exclude: /(node_modules)/
            },
            {
                test: /\.html$/,
                loader: "raw-loader"
            }
        ]
    },
    plugins: [
        new Webpack.optimize.UglifyJsPlugin(),
        new Webpack.optimize.CommonsChunkPlugin({
            name: "bundle-vendors"
        })
    ]
}